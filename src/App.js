import React, { Component } from 'react'
import Helpers from './Helpers'
import './App.css'

class App extends Component {
  constructor (props) {
    super(props)

    this.state = {
      userLocation: null,
      weatherData: null,
      dataLoaded: false,
      showWind: true,
      selectedTemperatureUnit: 'metric',
      selectedShowWind: 'on',
      widgetTitle: ''
    }
  }

  componentDidMount () {
    this.setState({
      userLocation: this.getUserLocation()
    })
  }

  getUserLocation () {
    return navigator.geolocation && navigator.geolocation.getCurrentPosition(this.geoLocSuccess.bind(this), this.geoLocError.bind(this), {
      enableHighAccuracy: false,
      timeout: 5000,
      maximumAge: 0
    })
  }

  async geoLocSuccess (pos) {
    const units = this.state.selectedTemperatureUnit

    const city = await Helpers.fetchWeatherData(pos, units)

    if (!city) {
      return
    }

    city.wind.metric = `${Helpers.windDirectionName(city.wind.deg)} ${Helpers.translateMetricWindSpeed(city.wind.speed)}`
    city.wind.imperial = `${Helpers.windDirectionName(city.wind.deg)} ${Helpers.translateImperialWindSpeed(city.wind.speed)}`

    this.setState({
      weatherData: city,
      dataLoaded: true
    })
  }

  geoLocError (err) {
    console.warn(`ERROR Fetching weather data (${err.code}): ${err.message}; retrying..`)
    // calls every 5 seconds for unlimited calls! it'd be nice to have a call limit
    this.getUserLocation()
  }

  changeTemperatureUnit (val) {
    this.setState({
      selectedTemperatureUnit: val,
      dataLoaded: false
    })

    this.getUserLocation()
  }

  changeWindVisibility (val) {
    this.setState({
      selectedShowWind: val
    })
  }

  changeWidgetTitle (el) {
    this.setState({
      widgetTitle: el.target.value
    })
  }

  render () {
    const weatherData = this.state.dataLoaded ? this.state.weatherData : false
    const widgetTitle = this.state.widgetTitle.toUpperCase()
    const selectedShowWind = this.state.selectedShowWind
    const selectedTemperatureUnit = this.state.selectedTemperatureUnit

    return (
      <div className='wrapper-box flex flex-justify-content-center flex-align-items-center pa-5'>
        <div className='widget-settings-wrapper ma-3 pa-5'>
          <div className='widget-settings'>
            <div className='title'>
              <label>
                <div className='pb-2'>Title</div>

                <input
                  className='input-title'
                  type='text'
                  placeholder='Title of widget'
                  name='title'
                  onChange={(e) => this.changeWidgetTitle(e)}
                />
              </label>
            </div>

            <div className='temperature mt-2'>
              <div className='py-2'>Temperature</div>

              <div className='flex fg-1'>
                <label>
                  <input
                    className='input-radio'
                    type='radio'
                    name='temperature'
                    value='metric'
                    onChange={(e) => this.changeTemperatureUnit('metric', e)}
                    checked={this.state.selectedTemperatureUnit === 'metric'}
                  />
                  &deg;C
                </label>

                <label>
                  <input
                    className='input-radio'
                    type='radio'
                    name='temperature'
                    value='imperial'
                    onChange={(e) => this.changeTemperatureUnit('imperial', e)}
                    checked={this.state.selectedTemperatureUnit === 'imperial'}
                  />
                  &deg;F
                </label>
              </div>
            </div>

            <div className='wind mt-2'>
              <div className='py-2'>Wind</div>

              <div className='flex fg-1 body-1'>
                <label>
                  <input
                    type='radio'
                    name='show_wind'
                    value='on'
                    onChange={(e) => this.changeWindVisibility('on', e)}
                    checked={this.state.selectedShowWind === 'on'}
                  />
                  On
                </label>

                <label>
                  <input
                    type='radio'
                    name='show_wind'
                    value='off'
                    onChange={(e) => this.changeWindVisibility('off', e)}
                    checked={this.state.selectedShowWind === 'off'}
                  />
                  Off
                </label>
              </div>
            </div>
          </div>
        </div>

        <div className='vertical-line' />

        <div className='widget-preview-wrapper ma-3 pa-5'>
          <div className='widget-preview box-shadow-5 br-1 pa-5'>
            <h3 className='text-uppercase mt-2 mb-5'>
              { widgetTitle || 'TITLE OF WIDGET' }
            </h3>

            {
              weatherData ? (
                <div className='flex mt-2'>
                  <div>
                    <img
                      className='d-block'
                      src={`http://openweathermap.org/img/w/${weatherData.weather[0].icon}.png`}
                      alt={weatherData.weather[0].description}
                      width='100'
                    />
                  </div>

                  <div className='pa-1'>
                    <div className='subheading'>{ weatherData.name }</div>
                    <div className='display-1 font-weight-bold'>{ Math.round(weatherData.main.temp) }&deg;</div>

                    { selectedShowWind === 'on' &&
                      <div className='caption'>
                        <span className='font-weight-bold'>Wind</span>
                        {
                          selectedTemperatureUnit === 'metric' ? (
                            <span className='ml-2'>
                              { weatherData.wind.metric }
                            </span>
                          ) : (
                            <span className='ml-2'>
                              { weatherData.wind.imperial }
                            </span>
                          )
                        }
                      </div>
                    }
                  </div>
                </div>
              ) : (
                <div className='flex mt-2'>Loading data..</div>
              )
            }
          </div>
        </div>
      </div>
    )
  }
}

export default App
