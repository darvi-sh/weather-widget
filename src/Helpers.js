export default class Helpers {
  static async fetchWeatherData (pos, units) {
    const crd = pos.coords

    const result = await fetch(
      `http://api.openweathermap.org/data/2.5/find?lat=${crd.latitude}&lon=${crd.longitude}&cnt=1&units=${units}&appid=ca9ef84fdda4e1a050d44887e1ee21d9`
    )
    const allCities = await result.json()

    if (!allCities.count) {
      return false
    }

    return allCities.list[0]
  }

  static MPSToKMH = (num = 0) => num * 3.6;

  static MPSToMPH = (num = 0) => (num * 2.23694).toFixed(2);

  static translateMetricWindSpeed = speed => `${Number.parseFloat(Helpers.MPSToKMH(speed)).toFixed(0)}km/h`;

  static translateImperialWindSpeed = speed => `${Number.parseFloat(Helpers.MPSToMPH(speed)).toFixed(0)}mph`;

  static windDirectionName(deg) {
    let direction;

    switch (true) {
      default:
      case (deg > 348.75 || deg < 11.25): direction = 'N'; break;
      case (deg > 11.25 || deg < 33.75): direction = 'NNE'; break;
      case (deg > 33.75 || deg < 56.25): direction = 'NE'; break;
      case (deg > 56.25 || deg < 78.75): direction = 'ENE'; break;
      case (deg > 78.75 || deg < 101.25): direction = 'E'; break;
      case (deg > 101.25 || deg < 123.75): direction = 'ESE'; break;
      case (deg > 123.75 || deg < 146.25): direction = 'SE'; break;
      case (deg > 146.25 || deg < 168.75): direction = 'SSE'; break;
      case (deg > 168.75 || deg < 191.25): direction = 'S'; break;
      case (deg > 191.25 || deg < 213.75): direction = 'SSW'; break;
      case (deg > 213.75 || deg < 236.25): direction = 'SW'; break;
      case (deg > 236.25 || deg < 258.75): direction = 'WSW'; break;
      case (deg > 258.75 || deg < 281.25): direction = 'W'; break;
      case (deg > 281.25 || deg < 303.75): direction = 'WNW'; break;
      case (deg > 303.75 || deg < 326.25): direction = 'NW'; break;
      case (deg > 326.25 || deg < 348.75): direction = 'NNW'; break;
    }

    return direction;
  }
}
